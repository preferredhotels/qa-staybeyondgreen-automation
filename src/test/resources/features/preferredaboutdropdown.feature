#Author: Preferred Hotel QA

@SmokeRegression
Feature: Feature to test links in About drop down

	@SmokeTest
  Scenario: Validate user is able to select options in About drop down
    Given browser is open
    And user is on staybeyondgreen page
    And user clicks on the About Drop Down 
    And user clicks on the Goal link
    And user clicks on the About Drop Down 
    And user clicks on the Purpose link
    And user clicks on the About Drop Down 
    And user clicks on the Sustainability link
    And user clicks on the About Drop Down 
    And user clicks on the Team link
