package stepdefinitions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;

public class AboutDropDownSteps {

	WebDriver driver = null;
	
	private long pageLoadTime = 5000; // in millis, allow the page to load before perform next action
	private int mDefWidth = 1908;
	private int mDefHeight = 1070;
	
	@SuppressWarnings("deprecation")
	@Given("browser is open")
	public void browser_is_open() {
		
		String projectPath = System.getProperty("user.dir");
		
		System.setProperty("webdriver.chrome.driver", projectPath+"/src/test/resources/drivers/chromedriver.exe");
		
		driver = new ChromeDriver();
		
		driver.manage().window().setPosition(new Point(0,0));
		
		Dimension d = new Dimension(mDefWidth,mDefHeight);
	    //Resize current window to the set dimension
	    driver.manage().window().setSize(d);
		
		driver.manage().timeouts().implicitlyWait(10,  TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(10,  TimeUnit.SECONDS);
		
	}

	@And("user is on staybeyondgreen page")
	public void user_is_on_staybeyondgreen_page() {
		driver.navigate().to("https://staybeyondgreen.com/");
	}

	@And("user clicks on the About Drop Down")
	public void user_clicks_on_the_about_drop_down() throws InterruptedException {
		driver.findElement(By.id("dropdownMenuLink")).click();
		Thread.sleep(2000);
	}

	@And("user clicks on the Goal link")
	public void user_clicks_on_the_goal_link() throws InterruptedException {
		performMouseOverAndClick("GOAL");
		Thread.sleep(pageLoadTime);
	}

	@And("user clicks on the Purpose link")
	public void user_clicks_on_the_purpose_link() throws InterruptedException {
		performMouseOverAndClick("PURPOSE");
		Thread.sleep(pageLoadTime);
	}

	@And("user clicks on the Sustainability link")
	public void user_clicks_on_the_sustainability_link() throws InterruptedException {
		performMouseOverAndClick("SUSTAINABILITY");
		Thread.sleep(pageLoadTime);
	}

	@And("user clicks on the Team link")
	public void user_clicks_on_the_team_link() throws InterruptedException {
		performMouseOverAndClick("TEAM");
		Thread.sleep(pageLoadTime);
		tearDown();
	}
	
	private void tearDown() {
		driver.close();
		driver.quit();
	}
	
	private void performMouseOverAndClick(String optionName) {
		WebElement elm = getDropDownOptionsElement(optionName);
		Actions actions = new Actions(driver);
		actions.moveToElement(elm).click(elm).build().perform();
	}
	
	private WebElement getDropDownOptionsElement(String optionName) {
		WebElement element = null;
		List<WebElement> options =    driver.findElements(By.xpath("//div[@class='dropdown-menu header__about__container show']"));
		for(WebElement option : options) {
        	System.out.println("option value: " + option.getText());
            if(option.getText().trim().contains(optionName)) {
            	element = driver.findElement(By.linkText(optionName));
            }
        }
		return element;
	}

}
